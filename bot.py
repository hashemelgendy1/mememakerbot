import discord
import responses
import os


async def send_message(message, user_message, is_private):
  try:
    response = responses.handle_response(user_message)
    await (message.author.send(response)
           if is_private else await message.channel.send(response))

  except Exception as e:
    print(e)


def run_discord_bot():
  TOKEN = "MTIzMTk5NjA2ODgwMDEwMjU0MQ.Gw6eTs.YZEchGyzvsjsUIArBE9wjtgVUdajS7RXtornJI"
  intents = discord.Intents.default()
  intents.message_content = True
  client = discord.Client(intents=intents)

  @client.event
  async def on_ready():
    print(f'{client.user} is now running!')
  @client.event
  async def on_message(message):
    if message.author == client.user:
      return
    username = str(message.author)
    user_message = str(message.content)
    channel = str(message.channel)

    print(f"Message {user_message} by {username} on {channel}")

    await send_message(message, user_message, is_private=False)

  client.run(TOKEN)
